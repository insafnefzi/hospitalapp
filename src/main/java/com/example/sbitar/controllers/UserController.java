package com.example.sbitar.controllers;

import com.example.sbitar.entities.User;
import com.example.sbitar.repositories.IUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/v2/users")
public class UserController {

    @Autowired
    private IUser userRepository;

    @PostMapping("/")
    public ResponseEntity createUser(@RequestBody User user){
        if (user ==null){
            return ResponseEntity.badRequest().body("cannot create user with empty fields");
        }
        User createdUser = userRepository.save(user);
        return ResponseEntity.ok(createdUser);
    }



}

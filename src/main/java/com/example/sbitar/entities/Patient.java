package com.example.sbitar.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "PATIENT")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Patient {
    @Id
    @GeneratedValue
    private  long idPatient;
    private String firstName;
    private String lastName;
    private int age;
    private String sexe;
    private String antecedant;
    private String habitude;
    private String diagnostic;

    public void setIdPatient(long idPatient) {
        this.idPatient = idPatient;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public void setAntecedant(String antecedant) {
        this.antecedant = antecedant;
    }

    public void setHabitude(String habitude) {
        this.habitude = habitude;
    }

    public void setDiagnostic(String diagnostic) {
        this.diagnostic = diagnostic;
    }

    public long getIdPatient() {
        return idPatient;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getSexe() {
        return sexe;
    }

    public String getAntecedant() {
        return antecedant;
    }

    public String getHabitude() {
        return habitude;
    }

    public String getDiagnostic() {
        return diagnostic;
    }
}

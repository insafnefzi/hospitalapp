package com.example.sbitar.repositories;

import com.example.sbitar.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUser extends JpaRepository<User, Long> {
}

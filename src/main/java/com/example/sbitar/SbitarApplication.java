package com.example.sbitar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbitarApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbitarApplication.class, args);
	}

}
